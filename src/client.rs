
use sha1;

use std::sync::{Arc, Mutex};
use std::fmt;
use std::error::Error;
use std::sync::mpsc;
use std::io::{Read, Write};

use std::collections::HashMap;
use http_muncher::Parser;
use rustc_serialize::base64::{ToBase64, STANDARD};

use mio::*;
use mio::net::TcpStream;
use mio::deprecated::Sender;


use http::HttpParser;
use frame::{WebSocketFrame, OpCode};
use interface::WebSocketEvent;


enum ClientState {
    AwaitingHandshake(Mutex<Parser<HttpParser>>),
    HandshakeResponse,
    Connected,
}

const WEBSOCKET_KEY: &'static [u8] = b"258EAFA5-E914-47DA-95CA-C5AB0DC85B11";



pub struct WebSocketClient {
    pub socket: TcpStream,

    headers: Arc<Mutex<HashMap<String, String>>>,
    pub interest: Ready,

    state: ClientState,
    outgoing: Vec<WebSocketFrame>,

    // 从客户端连接 socket 读取到数据后，给 WebSocket 对象处理
    // WebSocket 对象再把"回复"给客户端 socket
    tx: Mutex<mpsc::Sender<WebSocketEvent>>,
    // 用于给给 server 发消息，请求重新注册客户端，主要是修改 interest
    event_loop_tx: Sender<WebSocketEvent>,
    token: Token,
}


fn gen_key(key: &String) -> String {
    let mut m = sha1::Sha1::new();
    let mut buf = [0u8; 20];

    m.update(key.as_bytes());
    m.update(WEBSOCKET_KEY);
    m.output(&mut buf);
    return buf.to_base64(STANDARD);
}


impl WebSocketClient {
    pub fn new(
        socket: TcpStream,
        token: Token,
        server_sink: mpsc::Sender<WebSocketEvent>,
        event_loop_sink: Sender<WebSocketEvent>,
    ) -> WebSocketClient {
        let headers = Arc::new(Mutex::new(HashMap::new()));
        WebSocketClient {
            socket: socket,
            // we're making a first clone of the headers variable to read it's contents:
            headers: headers.clone(),
            interest: Ready::readable(),
            state: ClientState::AwaitingHandshake(Mutex::new(Parser::request(HttpParser {
                current_key: None,
                // and the second clone to write new headers to it:
                headers: headers.clone(),
            }))),
            outgoing: Vec::new(),
            tx: Mutex::new(server_sink),
            event_loop_tx: event_loop_sink,
            token: token,
        }
    }

    pub fn send_message(&mut self, msg: WebSocketEvent) -> Result<(), String> {
        let frame = match msg {
            WebSocketEvent::TextMessage(_, ref data) => Some(WebSocketFrame::from(&*data.clone())),
            WebSocketEvent::BinaryMessage(_, ref data) => Some(WebSocketFrame::from(data.clone())),
            WebSocketEvent::Close(_) => {
                let close_frame = WebSocketFrame::close(0, b"Server-initiated close")?;
                Some(close_frame)
            }
            WebSocketEvent::Ping(_, ref payload) => Some(WebSocketFrame::ping(payload.clone())),
            _ => None,
        };

        if frame.is_none() {
            return Err("Wrong message type to send".to_string());
        }

        // 这里不直接写frames，而是通知 eventloop WebSocketEvent::Reregister 消息
        // 该消息会在 handler.ready 中的events.is_writable 后调用
        self.outgoing.push(frame.unwrap());

        if self.interest.is_readable() {
            self.interest.insert(Ready::writable());
            self.interest.remove(Ready::readable());
            // 发消息给 server，请求重新注册客户端；重新注册客户端时主要是更新 interest
            self.event_loop_tx
                .send(WebSocketEvent::Reregister(self.token))
                .map_err(|e| e.description().to_string());
        }

        Ok(())
    }

    pub fn read(&mut self) {
        match self.state {
            ClientState::AwaitingHandshake(_) => self.read_handshake(),
            ClientState::Connected => self.read_frame(),
            _ => {}
        }
    }

    fn read_frame(&mut self) {
        let frame = WebSocketFrame::read(&mut self.socket);
        match frame {
            Ok(frame) => {

                match frame.get_opcode() {
                    OpCode::TextFrame => {
                        let payload = String::from_utf8(frame.payload).unwrap();
                        self.tx.lock().unwrap().send(WebSocketEvent::TextMessage(
                            self.token,
                            payload,
                        ));
                    }
                    OpCode::BinaryFrame => {
                        self.tx.lock().unwrap().send(WebSocketEvent::BinaryMessage(
                            self.token,
                            frame.payload,
                        ));
                    }
                    OpCode::Ping => {
                        self.outgoing.push(WebSocketFrame::pong(&frame));
                    }
                    OpCode::ConnectionClose => {
                        self.tx.lock().unwrap().send(
                            WebSocketEvent::Close(self.token),
                        );
                        self.outgoing.push(WebSocketFrame::close_from(&frame));
                    }
                    _ => {}
                }

                if self.outgoing.len() > 0 {
                    self.interest.remove(Ready::readable());
                    self.interest.insert(Ready::writable());
                }
            }
            Err(e) => println!("error while reading frame: {}", e),
        }
    }

    fn read_handshake(&mut self) {
        loop {
            println!("Waiting for handshake...");
            let mut buf = [0; 2048];
            match self.socket.read(&mut buf) {
                Ok(_) => {
                    let is_upgrade =
                        if let ClientState::AwaitingHandshake(ref parser_state) = self.state {
                            let mut parser = parser_state.lock().unwrap();

                            parser.parse(&buf);
                            parser.is_upgrade()
                        } else {
                            false
                        };

                    if is_upgrade {
                        self.state = ClientState::HandshakeResponse;
                        self.interest.remove(Ready::readable());
                        // 这里是握手，如果读到了 is_upgrade 的 header，就立即把 socket 设为写入并发送 handsake 的回复
                        self.interest.insert(Ready::writable());
                        println!("Prepare handshake write.");
                        break;
                    }
                }
                Err(e) => {
                    println!("Error while reading socket: {:?}.", e);
                    return;
                }
            }
        }
    }

    pub fn write(&mut self) {
        match self.state {
            ClientState::HandshakeResponse => self.write_handshake(),
            ClientState::Connected => self.write_frames(),
            _ => {}
        }
    }

    fn write_frames(&mut self) {
        let mut close_connection = false;

        {
            for frame in self.outgoing.iter() {
                println!("outgoing {:?}", frame);
                if let Err(e) = frame.write(&mut self.socket) {
                    println!("error on write: {}", e);
                }

                if frame.is_close() {
                    close_connection = true;
                }
            }
        }

        self.outgoing.clear();
        self.interest.remove(Ready::writable());
        self.interest.insert(if close_connection {
            Ready::hup()
        } else {
            Ready::readable()
        });
    }

    fn write_handshake(&mut self) {
        let headers = self.headers.lock().unwrap();

        let response_key = gen_key(&headers.get("Sec-WebSocket-Key").unwrap());
        let response = fmt::format(format_args!(
            "HTTP/1.1 101 Switching Protocols\r\n\
        Connection: Upgrade\r\n\
        Sec-WebSocket-Accept: {}\r\n\
        Upgrade: websocket\r\n\r\n",
            response_key
        ));
        self.socket.write(response.as_bytes()).unwrap();
        self.state = ClientState::Connected;
        self.interest.remove(Ready::writable());
        self.interest.insert(Ready::readable());
    }
}